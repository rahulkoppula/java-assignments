public class thisandsuper010304 {

    int num;
    String name;

    thisandsuper010304() {
        this(1, "Rahul");
    }

    thisandsuper010304 (int num, String name) {
        this.num = num;
        this.name = name;
        System.out.println(num + " " + name);
    }

    public static void main(String[] args) {
        thisandsuper010304 ts1 = new thisandsuper010304();
        thisandsuper010304 ts = new thisandsuper010304(2, "HYD");
    }
}
