public class thisandsuper06 {}
    class Parent_06 {
        void method1() {
            System.out.println("Used this() and super() in methods");
        }

        void method2() {
            this.method1();
        }
    }

    class Child_06 extends Parent_06 {
        void method3() {
            super.method2();
        }
    }

