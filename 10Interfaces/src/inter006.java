interface Jala_06 {
        default void defaultMethod() {
            System.out.println("This is a default method");
        }
    }

    class Interfaces_06 implements Jala_06 {
        public static void main(String[] args) {
            Interfaces_06 if6 = new Interfaces_06();
            if6.defaultMethod();
        }
}
