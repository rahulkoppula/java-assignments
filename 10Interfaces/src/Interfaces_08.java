    public interface Interfaces_08 {
        int num = 989; //values assigned

        public void myMethod();
    }

    class Jala_08 implements Interfaces_08 {
        @Override
        public void myMethod() {
            System.out.println("This is a method");
        }

        public static void main(String[] args) {
            Jala_08 j = new Jala_08();
            System.out.println(num);
            j.myMethod();
        }
}
