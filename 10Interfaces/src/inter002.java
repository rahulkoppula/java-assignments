public class inter002 {
    interface Jala_02 {
        void methodOne();

        void methodTwo();
    }

    public class Interfaces_02 implements Jala_02 {
        //cannot implement only one method in a class
        @Override
        public void methodOne() {
            System.out.println("First Method");
        }

        @Override
        public void methodTwo() {
            System.out.println("Second Method");
        }

        //main method
        public void main(String[] args) {
            Interfaces_02 if2 = new Interfaces_02();
            //Calling the methods implemented
            if2.methodOne();
            if2.methodTwo();
        }
    }
}
