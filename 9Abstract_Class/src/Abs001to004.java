    abstract class Animal {

        //non-abstract method
        void sleep() {
            System.out.println("Animal is sleeping");
        }
    }

    //subclass for an abstract class
    class Cat extends Animal {
        //provide implementation of abstract method
        private void sound() {
            System.out.println("Cat purrs");
        }

        public static void main(String[] args) {
            Animal A = new Cat();
            //accessing the non-abstract methods
            A.sleep();

            //instance for the child class
            Cat L = new Cat();
            //calling abstract methods
            L.sound();
            //calling non-abstract methods
            L.sleep();
        }
}
