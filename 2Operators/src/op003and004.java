import java.util.Scanner;
public class op003and004
{
        public static void main(String[] args)
        {
            Scanner num = new Scanner(System.in);
            System.out.print("Enter first number a: "); //
            int a = num.nextInt();
            System.out.print("Enter second number b: "); //
            int b = num.nextInt();
            // == means equal to
            if (a == b)
            {
                System.out.println("The numbers are equal");
            }
            // != means not equal to
            else if (a != b)
            {
                System.out.println("The numbers are not equal");
            }
        }
}
