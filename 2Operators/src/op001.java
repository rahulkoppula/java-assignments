public class op001 {

    static void addition(int a, int b)
    {
        int add = a + b;
        System.out.println("Sum of a + b = " + add);
    }

    static void subtraction(int a, int b)
    {
        int sub = a - b;
        System.out.println("Difference of a - b = " + sub);
    }

    static void multiplication(int a, int b)
    {
        int mul = a * b;
        System.out.println("Product of a * b = " + mul);
    }

    static void division(int a, int b)
    {
        int div = a / b;
        System.out.println("Reminder of a / b = " + div);
    }

    public static void main(String[] args)
    {
        //assigning values to int a and b
        int a = 50;
        int b = 25;
        //calling all methods
        addition(a, b);
        subtraction(a, b);
        multiplication(a, b);
        division(a, b);
    }
}
