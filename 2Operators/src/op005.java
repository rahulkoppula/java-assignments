public class op005 {
    public static void main(String[] args)
    {
        int a = 1;
        int b = 2;
        int c = 3;
        // AND operator
        System.out.println(a > b && a > c); // if one is false then answer is false
        System.out.println(a < b && a < c);
        System.out.println(a > b && a < c);

        // OR operator
        System.out.println(a > b || a > c); // if one is true then answer is true
        System.out.println(a < b || a < c);
        System.out.println(a < b || a > c);

        // NOT operator
        System.out.println(!(a > b));      // opposites
        System.out.println(!(a < b));
    }
}
