public class op002 {

    static void preIncrement(int a, int b) {
        int c;
        c = b + (++a);
        System.out.println("After Pre-Increment a,b are : " + a + "," + c);
    }

    static void postIncrement(int a, int b) {
        int d;
        d = b + (a++);
        System.out.println("After Post-Increment a,b are : " + a + "," + d);
    }

    static void preDecrement(int a, int b) {
        int e;
        e = b + (--a);
        System.out.println("After Pre-Decrement a,b are : " + a + "," + e);
    }

    static void postDecrement(int a, int b) {
        int f;
        f = b + (a--);
        System.out.println("After Post-Decrement a,b are: " + a + "," + f);
    }

    public static void main(String[] args) {
        int a = 20;
        int b = 40;
        preIncrement(a, b);
        postIncrement(a, b);
        preDecrement(a, b);
        postDecrement(a, b);
    }
}
