public class op006 {
    public static void main(String[] args) {
        int a = 3;
        int b = 9;
        System.out.println(a < b);  // < is less than
        System.out.println(a > b);  //> is greater than
        System.out.println(a <= b); //<= means less than or equal to
        System.out.println(a >= b); //>= means greater than or equal to
    }
}
