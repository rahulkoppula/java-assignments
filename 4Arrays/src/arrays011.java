import java.util.Arrays;

public class arrays011 {
    static void commonValues(int[] arr1, int[] arr2)
    {
        for (int i = 0; i < arr1.length; i++)
        {
            for (int j = 0; j < arr2.length; j++)
            {
                if (arr1[i] == arr2[j])
                {
                    System.out.println("Common numbers in the arrays : " + arr1[i]);
                }
            }
        }
    }

    public static void main(String[] args)
    {
        int[] arr1 = {22,33,44,55,66,77};
        int[] arr2 = {99,88,77,11,33,66};
        System.out.println("Array 1 = " + Arrays.toString(arr1));
        System.out.println("Array 2 = " + Arrays.toString(arr2));
        commonValues(arr1, arr2);
    }
}
