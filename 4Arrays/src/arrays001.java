public class arrays001
{
    //function to add integer values of an array
    static int arrSum(int[] arr)
    {
        int sum = 0;
        for (int i = 0; i < arr.length; i++)
            sum = sum + arr[i];
        return sum;
    }

    public static void main(String[] args)
    {
        int[] arr = {22, 33, 44, 55, 66};
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + " + ");
        System.out.print(" =  " + arrSum(arr));
    }
}
