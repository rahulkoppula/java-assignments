import java.util.Arrays;
import java.util.Scanner;

public class arrays003
{
    //method to find the index of an array
    static void arrIndex(int[] arr, int n)
    {
        for (int i = 0; i < arr.length; i++)
        {
            if (n == arr[i])
            {
                System.out.println(n + " is at index " + i);
                return;
            }
        }
    }

    public static void main(String[] args)
    {
        int n;
        int[] arr = {98, 87, 76, 65, 54, 43, 32, 21};
        System.out.println("arr = " + Arrays.toString(arr));
        System.out.print("Select any value to find Index of array : ");
        Scanner sc = new Scanner(System.in);
        n = sc.nextByte();
        arrIndex(arr, n);
    }
}
