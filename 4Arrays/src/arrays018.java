import java.util.Arrays;

public class arrays018 {
    static int removeDuplicate(int[] arr, int n)
    {
        if (n == 0 || n == 1)
        {
            return n;
        }
        int[] temp = new int[n];
        int j = 0;
        for (int i = 0; i < n - 1; i++)
        {
            if (arr[i] != arr[i + 1])
            {
                temp[j++] = arr[i];
            }
        }
        temp[j++] = arr[n - 1];
        if (j >= 0) System.arraycopy(temp, 0, arr, 0, j);
        return j;
    }

    public static void main(String[] args)
    {
        int[] arr = {11,22,33,44,55,66,55,44,33,22,11};
        System.out.println("Array = " + Arrays.toString(arr));
        int n = arr.length;

        n = removeDuplicate(arr, n);

        System.out.println("Array after removing duplicate values : ");
        for (int i = 0; i < n; i++)
            System.out.print(arr[i] + " ");
    }
}
