public class arrays002
{
    // to calculate the average of entered numbers
    static void avgArray(int[] arr)
    {
        int sum = 0;
        for (int i : arr)
        {
            sum += i;
        }
        int avg = sum / arr.length;
        System.out.print("The Average of entered array is " + avg);
    }

    public static void main(String[] args)
    {
        int[] arr = {12, 23, 34, 45, 56, 67};
        avgArray(arr);
    }
}
