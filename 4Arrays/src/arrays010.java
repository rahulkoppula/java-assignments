import java.util.Arrays;

public class arrays010 {
    static void duplicates(int[] arr)
    {
        for (int i = 0; i < arr.length - 1; i++)
        {
            for (int j = i + 1; j < arr.length; j++)
            {
                if ((arr[i] == arr[j] && (i != j)))
                {
                    System.out.println("Duplicate values : " + arr[j]);
                }
            }
        }
    }

    public static void main(String[] args)
    {
        int[] my_arr = {1,2,3,4,5,4,3,2,1};
        System.out.println("Array is : " + Arrays.toString(my_arr));

        duplicates(my_arr);
    }
}
