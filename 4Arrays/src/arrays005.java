import java.util.Arrays;
import java.util.Scanner;

public class arrays005
{
    static void removeElement(int[] arr, int n) //to delete an element from the array
    {
        for (int i = n; i < arr.length - 1; i++)
        {
            arr[i] = arr[i + 1];
        }
        System.out.println("New array generated : " + n );
        for (int i = 0; i < arr.length - 1; i++)
        {
            System.out.print(arr[i] + " ");
        }
    }

    public static void main(String[] args)
    {
        int[] my_arr = {10,20,30,40,50,60,70};
        System.out.print("Select the index to remove the number from array = " + Arrays.toString(my_arr) + " : ");
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        removeElement(my_arr, a);
    }
}
