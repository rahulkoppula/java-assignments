import java.util.Arrays;

public class arrasy013 {
    static void SecondGreates(int[] arr) {
        int temp;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        System.out.println("The second largest number in Array : " + arr[arr.length - 2]);
    }

    public static void main(String[] args) {
        int[] my_arr = {1,2,3,4,5,6,7,8,9};
        System.out.println("array = " + Arrays.toString(my_arr));

        SecondGreates(my_arr);
    }
}
