import java.util.Arrays;

//q14 is the same as q13
public class arrasy015 {
    static void even(int[] arr) {
        System.out.print("Even numbers in array : ");
        for (int i : arr) {
            if (i % 2 == 0) {
                System.out.print(i + "  ");
            }
        }
    }

    static void odd(int[] arr) {
        System.out.print(" \nOdd numbers in array : ");
        for (int j : arr) {
            if (j % 2 != 0) {
                System.out.print(j + "  ");
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {2,4,6,8,9,7,5,3,1};
        System.out.println("array = " + Arrays.toString(arr));

        even(arr);
        odd(arr);
    }
}
