import java.util.Arrays;

public class arrays016 {
    static void differenceofMin_Max(int[] arr) {
        int max = arr[0];
        int min = arr[0];
        int difference;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            } else if (arr[i] < min) {
                min = arr[i];
            }
        }
        difference = max - min;
        System.out.printf("Maximum value : %d , Minimum value : %d ", max, min);
        System.out.println("\nDifference of greatest and least numbers: " + difference);
    }

    public static void main(String[] args) {
        int[] my_arr = {1,2,3,4,5,6,5,4,3,9};
        System.out.println("Array = " + Arrays.toString(my_arr));

        differenceofMin_Max(my_arr);
    }
}
