import java.util.Arrays;
import java.util.Scanner;

public class arrays004
{
    //function to test if array contains a specific value
    static boolean contains(int[] arr, int n)
    {
        for (int i : arr)
        {
            if (i == n)
            {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args)
    {
        int n;
        int[] arr = {101, 102, 103, 104, 105, 106, 107};
        System.out.print("Enter the value to be searched for : ");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        boolean num = contains(arr, n);

        if (num)
        {
            System.out.println("The number " + n + " is in the array");
            System.out.println(Arrays.toString(arr));
        } else
        {
            System.out.println("The number " + n + " is not in the array");
            System.out.println(Arrays.toString(arr));
        }
    }
}
