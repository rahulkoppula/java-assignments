import java.util.Arrays;

public class arrays009 {
    static void ArrayReverse(int[] arr) {
        int[] r_arr = new int[arr.length];
        int x = arr.length;
        for (int i : arr) {
            r_arr[x - 1] = i;
            x = x - 1;
        }
        System.out.println("Generated reversed Array is : " + Arrays.toString(r_arr));
    }

    public static void main(String[] args) {
        int[] my_arr = {9,8,7,6,5,4,3,2,1};
        System.out.println("Original Array is :  " + Arrays.toString(my_arr));
        ArrayReverse(my_arr);
    }
}
