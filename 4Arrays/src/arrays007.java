import java.util.Arrays;
import java.util.Scanner;

public class arrays007
{
    static void InsertElement(int[] arr, int p, int v)
    {

        for (int i = arr.length; i <= p; i--)
        {
            arr[i] = arr[i - 1];
        }
        arr[p] = v;
    }

    public static void main(String[] args)
    {
        int p, v;    //p is index number , v is value
        int[] my_arr = {2,3,4,5,6,7,8,9,8,7,6,5,4,3,2};
        System.out.println("original Array : my_arr = " + Arrays.toString(my_arr));

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the index number : ");
        p = sc.nextInt();

        System.out.printf("Enter the value to insert at index my_arr[%d] =  ", p);
        v = sc.nextInt();

        InsertElement(my_arr, p, v);

        System.out.println("Array after Inserting Element : ");
        System.out.println("my_arr = " + Arrays.toString(my_arr));
    }
}
