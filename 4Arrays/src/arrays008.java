import java.util.Arrays;

public class arrays008
{
    static int minValue(int[] arr)
    {
        int min = arr[0];
        for (int i = 1; i < arr.length; i++)
            if (arr[i] < min)
                min = arr[i];
        return min;
    }

    static int maxValue(int[] arr)
    {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++)
            if (arr[i] > max)
                max = arr[i];
        return max;
    }

    public static void main(String[] args)
    {
        int[] my_arr = {222,333,444,555,666,777,888,999};
        System.out.println("my_arr = " + Arrays.toString(my_arr));
        System.out.println(" Maximum value in the arrays is : " + maxValue(my_arr));
        System.out.println(" Minimum value in the arrays is : " + minValue(my_arr));
    }
}
