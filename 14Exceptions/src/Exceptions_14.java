import java.lang.reflect.Method;

public class Exceptions_14 {
    public static void main(String[] args) {

        try {
            Class c = Class.forName("com.exceptions.E_14");
            Method m = c.getDeclaredMethod("subtraction", int.class, int.class);
        }
        catch (NoSuchMethodException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
