public class ex001002 {
    public static void main(String[] args) {
        int a = 50, b = 10, c;
        try {
            System.out.println("Inside try block");
            c = a / b;
            System.out.println("c = " + c);
        }
        catch (ArithmeticException e) {
            System.out.println("Handling the Arithmetic exception using try-catch block");
        }

        c = a / b;
        System.out.println("without exception handling");
    }
}

