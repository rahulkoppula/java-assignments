public class ex003 {
    static void throwException() {
        throw new RuntimeException("Throwing exception");
    }

    public static void main(String[] args) {
        throwException();
    }
}
