public class basics004 {
    public static void main(String[] args) {
        int i = 420;                // Default size of integer is 4 bytes
        float f = 15.3f;            // Default size of float type is 4 bytes
        double d = 123.4321;        // Default size of double is 8 bytes
        char c = 'R';               // Default size of char type is 2 bytes
        boolean b = true;           // Default size of boolean value is 1 byte

        System.out.println("Integer i is: " + i);
        System.out.println("Float f is: " + f);
        System.out.println("Double d is: " + d);
        System.out.println("Char c is: " + c);
        System.out.println("Boolean b is: " + b);
    }
}


