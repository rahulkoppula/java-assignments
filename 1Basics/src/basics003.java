public class basics003 {
    public static void main(String[] args) {
        System.out.println("Single line comment");       // This is a Single line comment

        System.out.println("Multi-line comment");        /* This is a Multi-line comment */

        System.out.println("Documentation comment");    /** This is a Documentation comment */
    }
}
