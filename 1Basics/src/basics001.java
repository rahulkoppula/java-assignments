// creating class
public class basics001 {
    //creating a method
    void isMethod() {
        //fields are used to store data
        String a = "This is a Method";
        System.out.println(a);
    }

    public static void main(String[] args) {
        // Created an object for basics001
        basics001 obj = new basics001();
        // method call line
        obj.isMethod();
    }
}
