public class basics005 {
    // Instance variable is declared in class but out of the body of method.
    int a = 1337;

    void localVariable() {
        //Local variable is declared in method
        int a = 8008;
        System.out.println("Local variable a is : " + a);
    }

    public static void main(String[] args) {

        basics005 obj = new basics005();
        System.out.println("Instance variable a is : " + obj.a);
        obj.localVariable();
    }
}
