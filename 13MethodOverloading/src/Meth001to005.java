public class Meth001to005 {

        int num1;
        int num2;
        int rollNo;
        String name;

        // 01.Two methods with the same name but different number of parameters of same type
        void method1(int num1) {
            this.num1 = num1;
            System.out.println("1.First number : " + num1);
        }

        void method1(int num1, int num2) {
            this.num1 = num1;
            this.num2 = num2;
            System.out.println("1.Sum of two numbers : " + (num1 + num2));
        }

        // 02.Two methods with the same name but different number of parameters of different data type
        void method2(int rollNo) {
            this.rollNo = rollNo;
            System.out.println("2.Roll number : " + rollNo);
        }

        void method2(int rollNo, String name) {
            this.rollNo = rollNo;
            this.name = name;
            System.out.println("2.Roll number : " + rollNo + " ; " + "Name : " + name);
        }

        // 04.Two methods with the same name and same number of parameters of different type
        void method4(int rollNo) {
            this.rollNo = rollNo;
            System.out.println("4.Roll number : " + rollNo);
        }

        void method4(String name) {
            this.name = name;
            System.out.println("4.Name : " + name);
        }

        public static void main(String[] args) {

            Meth001to005 mo = new Meth001to005 ();

            // 01.Calling the methods from main method
            mo.method1(11);
            mo.method1(10, 20);

            // 02.Calling the methods from main method
            mo.method2(23);
            mo.method2(23, "Rahul");

            // 03.Calling the methods from main method
            mo.method4(23);
            mo.method4("Rahul");
        }
    }
