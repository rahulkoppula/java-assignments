import java.util.Scanner;

public class loops010 {
    static boolean Palindrome(int x) {
        int r, sum = 0, temp;
        temp = x;
        while (x > 0) {
            r = x % 10;
            sum = (sum * 10) + r;
            x = x / 10;
        }
        return temp == sum;
    }

    public static void main(String[] args) {
        int num;
        System.out.print("Enter a number : ");
        num = new Scanner(System.in).nextInt();

        if (Palindrome(num)) {
            System.out.println(num + " is a Palindrome");
        } else {
            System.out.println(num + " is not a Palindrome");
        }
    }
}
