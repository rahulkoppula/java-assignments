public class loops013
{
    public static void main(String[] args)
    {

        int a = 10, b = 20, c = 30; // values in Q
        // Runs if a is greater than b & c
        if (a > b && a > c)
        {
            System.out.println(a + " is the largest number");

        }
        // Runs if b is greater than a & c
        else if (b > a && b > c)
        {
            System.out.println(b + " is the largest number");
        }
        // Runs if both a & b are smaller than c
        else
        {
            System.out.println(c + " is the largest number");
        }
    }
}
