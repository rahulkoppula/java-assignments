import java.util.Scanner;

public class loops003and004
{
        static void evenNum(int n)
        {
            System.out.println("Even Numbers from 0 to " + n);
            // If the remainder is zero then given number is even number
            for (int i = 1; i <= n; i++)
            {

                if (i % 2 == 0)
                {
                    System.out.print(i + " ");
                }
            }
            System.out.println(" ");
        }

        static void oddNum(int n)
        {
            // If the remainder is one then given number is odd number
            System.out.println("\nOdd Numbers from 1 to " + n);
            for (int i = 1; i <= n; i++)
            {

                if (i % 2 != 0)
                {
                    System.out.print(i + " ");
                }
            }
        }

        public static void main(String[] args)
        {
            int num;
            Scanner sc = new Scanner(System.in);
            System.out.print("Enter any Number : ");
            num = sc.nextInt();
            evenNum(num);
            oddNum(num);
        }
}
