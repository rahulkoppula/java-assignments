import java.util.Scanner;

public class loops005 {
    public static void main(String[] args) {

        int x, y, z;
        Scanner num = new Scanner(System.in);

        System.out.print("Enter First Number : ");
        x = num.nextInt();

        System.out.print("Enter Second Number : ");
        y = num.nextInt();

        System.out.print("Enter Third Number : ");
        z = num.nextInt();

        //works if x is greatest
        if ((x > y) && (x > z)) {
            System.out.println("The Largest Number is : " + x);
        }
        //works if y is greatest
        else if ((y > x) && (y > z)) {
            System.out.println("The Largest Number is : " + y);
        }
        //runs if neither x nor y are the greatest
        else {
            System.out.println("The Largest Number is : " + z);
        }
    }
}
