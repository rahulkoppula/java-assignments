public class loops006 {
    public static void main(String[] args) {

        System.out.println("Even Numbers from 10 to 100 are : ");
        int i = 10;
        while (i <= 100) {
            System.out.println(i);
            i += 2;
        }
    }
}
