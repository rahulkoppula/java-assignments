import java.util.Scanner;

public class loops009
{
    static void PrimeNumber(int n)
    {
        boolean Prime = false;
        for (int i = 2; i <= n / 2; ++i)
        {
            if (n % i == 0)
            {
                Prime = true;
                break;
            }
        }
        if (!Prime)
        {
            System.out.println(n + " is a Prime Number");
        } else
        {
            System.out.println(n + " is not a Prime Number");
        }
    }

    public static void main(String[] args) {
        int x;
        System.out.print("Enter number to check if prime number or not: ");
        x = new Scanner(System.in).nextInt();
        //calling method
        PrimeNumber(x);
    }
}
