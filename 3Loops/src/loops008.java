import java.util.Scanner;

public class loops008
{
    static boolean Armstrong(int n)
    {
        int r, result = 0;
        int orig = n;
        while (n != 0)
        {
            r = n % 10;
            /* A number is thought of as an Armstrong number if the sum of its own
               digits raised to the power number of digits gives the number itself.
               Eg: 153 = 1^3 + 5^3 + 3^3 ==> 1 + 125 + 27 ==> 153 */
            result += (r * r * r *r * r * r * r);
            n = n / 10;
        }
        return orig == result;
    }

    public static void main(String[] args)
    {

        System.out.print("Enter a number : ");
        int num = new Scanner(System.in).nextInt(); //Input is num

        if (Armstrong(num))
        {
            System.out.println(num + " is an Armstrong number");
        } else
        {
            System.out.println(num + " is not an Armstrong number");
        }
    }
}
