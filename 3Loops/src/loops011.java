import java.util.Scanner;

public class loops011
{
    public static void main(String[] args)
    {
        int a;
        System.out.print("Enter a number : ");
        a = new Scanner(System.in).nextInt();

        switch (a % 2) {
            case 0 -> System.out.println(a + " is an Even Number");
            case 1 -> System.out.println(a + " is an Odd Number");
        }
    }
}
