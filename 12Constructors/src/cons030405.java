public class cons030405 {

    int rollNo;
    String name;
    String clg;
    String city;

    //default access modifier
    cons030405() {
        this(23);
        System.out.println("Student Details");
    }

    //public access modifier
    public cons030405(int rollNo) {
        this("Rahul");
        this.rollNo = rollNo;
        System.out.println("rollNo : " + rollNo);
    }

    //private access modifiers
    private cons030405(String name) {
        this("ICFAI", "bowenpally");
        this.name = name;
        System.out.println("Name : " + name);
    }

    //protected access modifiers
    protected cons030405(String clg, String city) {
        this.clg = clg;
        this.city = city;
        System.out.println("college : " + clg);
        System.out.println("City : " + city);
    }

    public static void main(String[] args) {
        //calling the constructor multiple times with the same object
        cons030405 c = new cons030405();
    }
}