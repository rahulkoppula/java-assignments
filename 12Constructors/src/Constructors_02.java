public class Constructors_02 {
    public static void main(String... args) {
        new ChildClass_02();
        new ChildClass_02(23);
        new ChildClass_02("Rahul", "CSE", "ICFAI", "Hyderabad");
    }
}
