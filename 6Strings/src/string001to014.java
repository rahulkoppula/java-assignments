public class string001to014 {
    public static void main(String[] args) {

// Q1. Different ways creating a string
        String s1 = "This happens to be a string";
        System.out.println(s1);

        String s2 = new String("this also happens to be a string");
        System.out.println(s2);


// Q2. Concatenating two strings using + operator
        String scon1 = "Better word for concatenating ";
        String scon2 = "is linking";
        String con = scon1 + scon2;
        System.out.println(con);


// Q3. Finding the length of the string
        String sL = "This is for string Length";
        int len = sL.length();
        System.out.println("String Length is " + len);


// Q4. Extract a string using Substring
        String es = "Extracting is deleting letters at those indices";
        String sub = es.substring(5, 43);
        System.out.println("Extracted string : " + sub);


// Q5. Searching in strings using indexOf()
        String si = "java is a pain ";
        int i1 = si.indexOf("pain");
        int i2 = si.indexOf("is", 3);
        System.out.println("At Index : " + i1);
        System.out.println("At Index : " + i2);


// Q6. Matching a String Against a Regular Expression With matches()
        String ms = "This is basically find the word";
        boolean match = ms.matches("(.*)basically(.*)");
        System.out.println("Matches : " + match);


/* Q7. Comparing strings using the methods equals(), equalsIgnoreCase(),
      startsWith(), endsWith() and compareTo()
 */
        String Is1 = "Identical string";
        String Is2 = "Identical string";

        //two strings are compared using equals() method
        boolean e = Is1.equals(Is2);
        System.out.println("Equal : " + e);

        //equalsIgnoreCase() method ignores case differences
        boolean eic = Is1.equalsIgnoreCase(Is2);
        System.out.println("Equal(ignore case) : " + eic);

        //startsWith() method checks string begins
        boolean sw = Is1.startsWith("Comp");
        System.out.println("Starts with : " + sw);

        //endsWith() method checks string ending
        boolean ew = Is1.endsWith("ings");
        System.out.println("Ends with : " + ew);

        //compareTo() method returns 0 if both are equal
        int ct = Is1.compareTo(Is2);
        System.out.println("Compared : " + ct);


// Q9. Trimming strings with trim()
        String ts = "   Trimming   this    fat    string ";
        String trm = ts.trim();
        System.out.println("Trimmed : " + trm);


// Q10. Replacing characters in strings with replace()
        String rs = "Find and replace string";
        String rpl = rs.replace("i", "u");
        System.out.println("Replaced : " + rpl);


// Q11. Splitting strings with split()
        String ss = "Breaking this string apart";
        String[] spl = ss.split("s s"); //splits space

        for (String i : spl) {
            System.out.println("Splitted : " + i);
        }


// Q12. Converting Numbers to Strings with valueOf()
        int ns = 55;
        String numT0str = String.valueOf(ns);
        System.out.println("Converted Num to Str : " + numT0str + 22);


// Q13. Converting integer objects to Strings
        int is = 66;
        String intTstr = Integer.toString(is);
        System.out.println("Converted int to str : " + intTstr + 33);


// Q14. Converting to uppercase and lowercase
        String a = "upper case";
        String b = "LOWER CASE";
        String c = a.toUpperCase();
        String d = b.toLowerCase();
        System.out.println("Converted to upper case : " + c);
        System.out.println("Converted yo lower case : " + d);
    }
}
