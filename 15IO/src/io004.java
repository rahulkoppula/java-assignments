import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

public class io004 {
    public static void main(String[] args) {
        String s = "This text is written in the txt file using BufferedOutputStream in java.";

        try {
            // Creating a FileOutputStream
            FileOutputStream fos = new FileOutputStream("io004.txt");
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            byte[] arr = s.getBytes();
            bos.write(arr);
            bos.close();
            fos.close();
            System.out.println("Data is written in the file io004.txt .");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
