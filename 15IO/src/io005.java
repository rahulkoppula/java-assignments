import java.io.FileReader;

public class io005 {
    public static void main(String[] args) {

        try {
            FileReader fr = new FileReader("io004.txt"); // txt file from previous Q

            int x;
            while ((x = fr.read()) != -1) {
                System.out.print((char) x);
            }
            fr.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
