import java.io.FileWriter;

public class io006 {
    public static void main(String[] args) {

        String s = "This text is written using FileWriter in java.";

        try {
            // Creating FileWriter
            FileWriter fw = new FileWriter("io006.txt");

            fw.write(s);
            //closing FileWriter
            fw.close();
            System.out.println("Data is written to the file.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
