import java.io.FileInputStream;
import java.util.Properties;

public class io009 {
    public static void main(String args[]) {
        Properties prop = readPropertiesFile("propfile.txt");
        System.out.println("name: " + prop.getProperty("name"));
        System.out.println("City: " + prop.getProperty("City"));
        System.out.println("Qualification: " + prop.getProperty("Qualification"));
    }

    public static Properties readPropertiesFile(String fileName) {
        Properties prop = null;
        try {

            //Instantiating the FileInputStream for output file
            FileInputStream fis = new FileInputStream(fileName);
            prop = new Properties();
            prop.load(fis);
            fis.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return prop;
    }
}
