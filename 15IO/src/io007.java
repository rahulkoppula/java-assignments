import java.io.BufferedReader;
import java.io.FileReader;

public class io007 {
    public static void main(String args[]) {

        try {
            FileReader fr = new FileReader("io006.txt"); //txt from previous Q
            BufferedReader br = new BufferedReader(fr);

            int x;
            while ((x = br.read()) != -1) {
                System.out.print((char) x);
            }
            br.close();
            fr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
