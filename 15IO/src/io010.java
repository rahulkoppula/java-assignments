import java.io.FileOutputStream;
import java.util.Properties;

public class io010
{
    public static void main(String args[])
    {

        try
        {
            Properties props = new Properties();
            props.put("Name", "Rahul");
            props.put("City", "Hyd");
            props.put("Qualification", "Btech");

            FileOutputStream outputStrem = new FileOutputStream("propfilecreate.txt");
            props.store(outputStrem, "This here is a sample properties file");
            System.out.println("Properties file created in the designated folder");

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
