import java.io.FileInputStream;
import java.io.InputStream;

public class io001 {
    public static void main(String[] args) {

        try {
            InputStream fis = new FileInputStream("os.txt");
            int a;
            while ((a = fis.read()) != -1) {
                System.out.print((char) a);
            }
            fis.close();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
