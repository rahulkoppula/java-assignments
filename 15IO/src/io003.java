import java.io.BufferedInputStream;
import java.io.FileInputStream;

public class io003 {
    public static void main(String[] args) {

        try {
            // Creating a FileInputStream
            FileInputStream fis = new FileInputStream("io003.txt");
            // Creating a BufferedInputStream
            BufferedInputStream bis = new BufferedInputStream(fis);

            int x;
            while ((x = bis.read()) != -1) {
                System.out.print((char) x);
            }
            bis.close();
            fis.close();

        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
