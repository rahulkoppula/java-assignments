import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class Coll001 {
    public static void main(String[] args) {

        ArrayList<String> myList = new ArrayList<>(
                Arrays.asList("zero", "one", "two", "three", "four", "five"));
        System.out.println("\nmyList = " + myList);

        myList.add("six");
        myList.add("seven");
        myList.add("eight");
        myList.add("nine");
        System.out.println("\nAfter adding elements :");
        System.out.println("myList = " + myList);


        Iterator<String> itr = myList.iterator();
        System.out.println("\nIterating through the ArrayList : ");
        while (itr.hasNext()) {
            System.out.print(itr.next() + " ");
        }
        System.out.println(" ");

        myList.add(1, "AtIndex1");
        System.out.println("\nAfter adding an element at index 1 :");
        System.out.println("myList = " + myList);

        myList.remove(1);
        System.out.println("\nAfter removing an element at index 1 :");
        System.out.println("myList = " + myList);

        myList.set(1, "ONE");
        System.out.println("\nAfter updating the element at index 1 :");
        System.out.println("myList = " + myList);

        System.out.println("\nThe element is present at index :");
        System.out.println(myList.indexOf("three"));

        System.out.println("\nThe element at index 3 :");
        System.out.println(myList.get(3));

        System.out.println("\nSize of the ArrayList : ");
        System.out.println(myList.size());

        System.out.println("\n'two' is present in the ArrayList : ");
        System.out.println(myList.contains("two")); //returns boolean value

        System.out.println("\nAfter removing all elements of the ArrayList :");
        myList.clear();
        System.out.println("myList = " + myList);
    }
}
