import java.util.HashSet;
import java.util.Iterator;

public class coll003 {
    public static void main(String[] args) {

        HashSet<String> mySet = new HashSet<>();

        mySet.add("zero");
        mySet.add("one");
        mySet.add("two");
        mySet.add("three");
        mySet.add("four");
        mySet.add("five");
        mySet.add("six");
        mySet.add("seven");
        mySet.add("eight");
        mySet.add("nine");
        mySet.add("ten");
        mySet.add("nine");
        System.out.println("\nAfter adding elements to HashSet :");
        System.out.println("mySet = " + mySet);


        Iterator<String> itr = mySet.iterator();
        System.out.println("\nIterating through the HashSet : ");
        while (itr.hasNext()) {
            System.out.print(itr.next() + " ");
        }
        System.out.println(" ");

        System.out.println("\nclone/copy of HashSet : ");
        System.out.println(mySet.clone());

        mySet.remove("zero");
        System.out.println("\nAfter removing element 'zero' :");
        System.out.println("mySet = " + mySet);

        System.out.println("\nChecking if the set is empty :");
        System.out.println(mySet.isEmpty());


        System.out.println("\nSize of the HashSet : ");
        System.out.println(mySet.size());

        System.out.println("\nElement 'one' is present in the HashSet : ");
        System.out.println(mySet.contains("one"));

        System.out.println("\nAfter removing all elements of the HashSet :");
        mySet.clear();
        System.out.println("mySet = " + mySet);
    }
}
