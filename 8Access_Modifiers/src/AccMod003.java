public class AccMod003 {
    public class AccessModifiers003 {
        //Protected field
        protected String name;

        //Protected method
        protected void protectedMethod() {
            System.out.println("This is a " + name);
        }
    }

    //Accessing protected fields and methods from other class in the same package
    class protectedClass_03 {
        public void main(String[] args) {

            AccessModifiers003 pc = new AccessModifiers003();
            pc.name = "Protected Method";
            pc.protectedMethod();
        }
    }
}
