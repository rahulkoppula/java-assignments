public class AccMod002 {
    static class DefaultClass002 {

        //Default fields
        int myAge;
        String myName;

        //Default method
        void defaultMethod() {
            System.out.println("My name is " + myName + " and my age is " + myAge);
        }
    }

    //Accessing default fields and method from other class in the same package
    public static class AccessModifiers002 {
        public static void main(String[] args) {
            DefaultClass002 obj = new DefaultClass002();
            obj.myAge = 21;
            obj.myName = "Rahul";
            obj.defaultMethod();
        }
    }

}
