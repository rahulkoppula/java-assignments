public class AccMod001
{
    //private fields
    private String name = "Rahul";
    private int age = 21;

    //private method
    private void pvtMethod() {
        System.out.println("My Name is " + name + " and my age is " + age);
    }

    //main method
    public static void main(String[] args) {

        AccMod001 obj = new AccMod001();
        System.out.println(obj.age);
        System.out.println(obj.name);
        obj.pvtMethod();
    }
}


