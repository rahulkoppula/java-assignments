//Accessing the public methods and fields from other class in the same package
public class AccessModifiers_04 {
    public void main(String[] args) {
        //Creating object of PublicClass_04.
        PublicClass_04 pub = new PublicClass_04();
        pub.pName = "Public Method";
        //Calling method
        pub.publicMethod();
    }
}
