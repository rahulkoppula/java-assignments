public class static001to007 {

    static int s1 = 12;
    static int s2 = 21;

    int i1 = 56;
    int i2 = 65;

    // Q2. Prints Instance variables in Static method
    static void staticMethod1() {
        static001to007 obj = new static001to007();
        System.out.println("Instance variables: " + obj.i1 + ", " + obj.i2);
    }

    // Q3. Prints Static variables in Instance method
    void instanceMethod1() {
        System.out.println("Static variables: " + s1 + ", " + s2);
    }

    // Q4. Calls Instance method 1 in Static method
    static void staticMethod2() {
        //can't call instance methods directly,so creating an object
        static001to007 obj = new static001to007();
        obj.instanceMethod1();
    }

    // Q5. Call Static methods in Instance method
    void instanceMethod2() {
        staticMethod1();
    }

    public static void main(String[] args) {
        static001to007 stc = new static001to007();

        // Q8. Print all the static, instance variables in main method
        System.out.println("Static variables: " + s1 + ", " + s2);
        System.out.println("Instance variables: " + stc.i1 + ", " + stc.i2);

        // Q7.Call static methods and instance methods in main method
        staticMethod1();
        staticMethod2();
        stc.instanceMethod1();
        stc.instanceMethod2();
    }
}
